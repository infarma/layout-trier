## Arquivo de Pedidos Versão 04/16
gerador-layouts arquivoDePedido pedidoItens IdentificacaoLayout:string:0:2 CodigoCliente:int32:2:10 NumeroReservado:int64:10:20 CodigoBarrasProduto:int64:20:33 QuantidadePedidaProduto:int32:33:38 CodigoCondicaoPagamento:int32:38:41 DataNegociacao:int32:41:49 NumeroPedidoCliente:int64:49:59 UFFornecedor:string:59:61 CodigoFilialFornecedora:string:61:65 UFCliente:string:65:67 Reservado:string:67:114 NumeroSequencialArquivo:int32:114:120


## Retorno de Pedidos Versão 04/16
gerador-layouts retornoDePedidos retornoItens IdentificacaoLayout:string:0:2 CodigoCliente:int32:2:10 NumeroReservado:int64:10:20 CodigoBarrasProduto:int64:20:33 QuantidadeFaltas:int32:33:38 CodigoRetornoFalta:int32:38:41 DescricaoRetornoFalta:string:41:81 NumeroPedidoCliente:int64:81:91 Reservado:string:91:134 NumeroSequencialArquivo:int32:134:140


## Arquivo de Pedidos Versão 03/16
gerador-layouts arquivoDePedido header TipoRegistro:int32:0:1 CodigoCliente:string:1:16 NumeroPedido:string:16:28 DataPedido:int32:28:36 TipoCompra:string:36:37 TipoRetorno:string:37:38 ApontadorCondicaoComercial:int32:38:44 CampoLivre:string:44:94

gerador-layouts arquivoDePedido detalhe TipoRegistro:int32:0:1 NumeroPedido:string:1:13 CodigoProduto:int64:13:26 Quantidade:int32:26:31 Desconto:float32:31:36:2

gerador-layouts arquivoDePedido trailler TipoRegistro:int32:0:1 NumeroPedido:string:1:13 QuantidadeUnidades:int32:13:18 QuantidadeItens:int64:18:28


## Retorno de Pedidos Versão 03/16
gerador-layouts retornoDePedido header TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedidoCliente:string:16:28 DataProcessamento:int32:28:36 HoraProcessamento:int32:36:44 NumeroPedidoOL:int64:44:56 CodigoMotivo:int32:56:59 DescricaoMotivo:string:59:109 CampoLivre:string:109:159

gerador-layouts retornoDePedido detalhe TipoRegistro:int32:0:1 CodigoProduto:int64:1:14 NumeroPedido:string:14:26 CondicaoPagamento:string:26:27 QuantidadeAtendida:int32:27:32 DescontoAplicado:float32:32:37:2 PrazoConcedido:int32:37:40 QuantidadeNaoAtendida:int32:40:45 CodigoMotivo:int32:45:48 DescricaoMotivo:string:48:98

gerador-layouts retornoDePedido trailler TipoRegistro:int32:0:1 NumeroPedido:string:1:13 QuantidadeLinhas:int32:13:18 QuantidadeItensAtendidos:int64:18:23 QuantidadeItensNaoAtendidos:int32:23:28