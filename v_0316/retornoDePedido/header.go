package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	TipoRegistro        int32  `json:"TipoRegistro"`
	CnpjFarmacia        string `json:"CnpjFarmacia"`
	NumeroPedidoCliente string `json:"NumeroPedidoCliente"`
	DataProcessamento   int32  `json:"DataProcessamento"`
	HoraProcessamento   int32  `json:"HoraProcessamento"`
	NumeroPedidoOL      int64  `json:"NumeroPedidoOL"`
	CodigoMotivo        int32  `json:"CodigoMotivo"`
	DescricaoMotivo     string `json:"DescricaoMotivo"`
	CampoLivre          string `json:"CampoLivre"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjFarmacia, "CnpjFarmacia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataProcessamento, "DataProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraProcessamento, "HoraProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoOL, "NumeroPedidoOL")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoMotivo, "CodigoMotivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DescricaoMotivo, "DescricaoMotivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CampoLivre, "CampoLivre")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":        {0, 1, 0},
	"CnpjFarmacia":        {1, 16, 0},
	"NumeroPedidoCliente": {16, 28, 0},
	"DataProcessamento":   {28, 36, 0},
	"HoraProcessamento":   {36, 44, 0},
	"NumeroPedidoOL":      {44, 56, 0},
	"CodigoMotivo":        {56, 59, 0},
	"DescricaoMotivo":     {59, 109, 0},
	"CampoLivre":          {109, 159, 0},
}
