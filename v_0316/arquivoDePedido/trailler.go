package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	NumeroPedido       string `json:"NumeroPedido"`
	QuantidadeUnidades int32  `json:"QuantidadeUnidades"`
	QuantidadeItens    int64  `json:"QuantidadeItens"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeUnidades, "QuantidadeUnidades")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItens, "QuantidadeItens")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"NumeroPedido":       {1, 13, 0},
	"QuantidadeUnidades": {13, 18, 0},
	"QuantidadeItens":    {18, 28, 0},
}
