package layout_trier

import (
	arquivoDePedidoV0316 "bitbucket.org/infarma/layout-trier/v_0316/arquivoDePedido"
	arquivoDePedidoV0416 "bitbucket.org/infarma/layout-trier/v_0416/arquivoDePedido"
	"os"
)

func GetArquivoDePedidoV0416(fileHandle *os.File) (arquivoDePedidoV0416.ArquivoDePedido, error) {
	return arquivoDePedidoV0416.GetStruct(fileHandle)
}

func GetArquivoDePedidoV00316(fileHandle *os.File) (arquivoDePedidoV0316.ArquivoDePedido, error) {
	return arquivoDePedidoV0316.GetStruct(fileHandle)
}
