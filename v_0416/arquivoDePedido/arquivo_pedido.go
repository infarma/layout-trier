package v_0416

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	PedidoItens []PedidoItens `json:"PedidoItens"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	var index int32
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		err = arquivo.PedidoItens[index].ComposeStruct(string(runes))
	}
	return arquivo, err
}
