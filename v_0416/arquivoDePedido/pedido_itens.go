package v_0416

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type PedidoItens struct {
	IdentificacaoLayout     string `json:"IdentificacaoLayout"`
	CodigoCliente           int32  `json:"CodigoCliente"`
	NumeroReservado         int64  `json:"NumeroReservado"`
	CodigoBarrasProduto     int64  `json:"CodigoBarrasProduto"`
	QuantidadePedidaProduto int32  `json:"QuantidadePedidaProduto"`
	CodigoCondicaoPagamento int32  `json:"CodigoCondicaoPagamento"`
	DataNegociacao          int32  `json:"DataNegociacao"`
	NumeroPedidoCliente     int64  `json:"NumeroPedidoCliente"`
	UFFornecedor            string `json:"UFFornecedor"`
	CodigoFilialFornecedora string `json:"CodigoFilialFornecedora"`
	UFCliente               string `json:"UFCliente"`
	Reservado               string `json:"Reservado"`
	NumeroSequencialArquivo int32  `json:"NumeroSequencialArquivo"`
}

func (p *PedidoItens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPedidoItens

	err = posicaoParaValor.ReturnByType(&p.IdentificacaoLayout, "IdentificacaoLayout")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroReservado, "NumeroReservado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoBarrasProduto, "CodigoBarrasProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadePedidaProduto, "QuantidadePedidaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DataNegociacao, "DataNegociacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.UFFornecedor, "UFFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoFilialFornecedora, "CodigoFilialFornecedora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.UFCliente, "UFCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Reservado, "Reservado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroSequencialArquivo, "NumeroSequencialArquivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesPedidoItens = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificacaoLayout":     {0, 2, 0},
	"CodigoCliente":           {2, 10, 0},
	"NumeroReservado":         {10, 20, 0},
	"CodigoBarrasProduto":     {20, 33, 0},
	"QuantidadePedidaProduto": {33, 38, 0},
	"CodigoCondicaoPagamento": {38, 41, 0},
	"DataNegociacao":          {41, 49, 0},
	"NumeroPedidoCliente":     {49, 59, 0},
	"UFFornecedor":            {59, 61, 0},
	"CodigoFilialFornecedora": {61, 65, 0},
	"UFCliente":               {65, 67, 0},
	"Reservado":               {67, 114, 0},
	"NumeroSequencialArquivo": {114, 120, 0},
}
