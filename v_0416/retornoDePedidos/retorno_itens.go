package retornoDePedidos

type RetornoItens struct {
	IdentificacaoLayout     string `json:"IdentificacaoLayout"`
	CodigoCliente           int32  `json:"CodigoCliente"`
	NumeroReservado         int64  `json:"NumeroReservado"`
	CodigoBarrasProduto     int64  `json:"CodigoBarrasProduto"`
	QuantidadeFaltas        int32  `json:"QuantidadeFaltas"`
	CodigoRetornoFalta      int32  `json:"CodigoRetornoFalta"`
	DescricaoRetornoFalta   string `json:"DescricaoRetornoFalta"`
	NumeroPedidoCliente     int64  `json:"NumeroPedidoCliente"`
	Reservado               string `json:"Reservado"`
	NumeroSequencialArquivo int32  `json:"NumeroSequencialArquivo"`
}
